package com.ingress.docker1.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

    @GetMapping("/api/v1/hello2")
    public String hello() {
        return "Hello from container 2";
    }
}
