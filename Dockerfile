FROM openjdk:21-jdk
WORKDIR /app
COPY build/libs/*SNAPSHOT.jar /app/my-app.jar
ENTRYPOINT ["java"]
CMD ["-jar", "my-app.jar"]
